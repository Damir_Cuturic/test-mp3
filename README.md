# Test-MP3.ps1

## Purpose

This script checks if the mp3 files on the Musicdrive (F:\) have the same SHA256 Hash as the files on the external location (P:\)

## Input 
A CSV file made by [dupeGuru](https://www.hardcoded.net/dupeguru/) with the following headers: 
``` 
"GroupID","Filename","Folder","Size","Kind","Match%
```


## TODO

[ ] Collect files with the same name (dupeGuru collects 100% matching files)

[ ] Check only first part of the file before moving on (speeding things up)

[ ] Test if External Harddrive is connected Output error message

[ ] Point script to CSV

[ ] Let the script collect the files (Yet another duplicate checker)
