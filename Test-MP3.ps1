# Test if Log folder exists
$log = Test-Path -Path .\log\test.log
if ($log -eq $FALSE){
    Push-Location
    Set-Location .\log
    New-Item -Path . -ItemType File -Name test.log
    Pop-Location
}

$p = Import-Csv -Path C:\Users\Gebruiker\Desktop\MP3_compare_.csv -Delimiter "," -Header "GroupID","Filename","Folder","Size","Kind","Match%"
<#
$p[0] is the HEADER form file
$p[1], $p[2] are first group
this means that inside a for loop $reference = 1 and $compare = 2
                                  $reference = 3 and $compare = 4
                                  $reference = 5 and $compare = 6
                                  $reference = 7 and $compare = 8
so $reference and $compare are not + 1 but *+2*
#>

foreach ($hit in $p){
    $FPath = $FPath + @($hit.Folder +"\"+ $hit.Filename)
}
<#
Same as above $FPath[0] = Folder\Filename
$FPath[1] and $FPath[2] = GroupID[1]
You can verify this by running 
PS C:\> $FPath | Out-GridView
#>
for ($i=1;$i -le ($FPath.Count - 2);$i + 2){
    $g = $i+1
    $reference = Get-FileHash -Path $FPath[$i] -Algorithm SHA256
    $compare = Get-FileHash -Path $FPath[$g] -Algorithm SHA256
    if($reference.Hash -ne $compare.Hash){
    Add-Content -Path ".\log\test.log" -Value "Failed" $FPath[$1] , $FPath[$g]
    }
}

# Comparing the files